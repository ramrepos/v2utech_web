import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LeftNavigationComponent } from './components/left-navigation/left-navigation.component';
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { CollaborationComponent } from './components/collaboration/collaboration.component';
import { HomeComponent } from './components/home/home.component';
import { UserDetailCard } from './components/userdetail-card/userdetail-card.component';
import { ResidentsComponent } from './components/residents/residents.component';

const appRoutes: Routes = [
  {
    path: '', 
    component: HomeComponent
  },
  { 
    path: 'collaboration', 
    component: CollaborationComponent 
  },
  { 
    path: 'dashboard', 
    component: BodyComponent 
  },
  {
    path: 'residents',
    component: ResidentsComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LeftNavigationComponent,
    HeaderComponent,
    BodyComponent,
    CollaborationComponent,
    HomeComponent,
    UserDetailCard,
    ResidentsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes,
      { enableTracing: true }),
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
